==Amazon scheduled purchase tool==

* The project requires another project [amazon-tracker] and in particular its module [amazon-tracker-purchase] to be present during building process.

* To run the program use the following syntax:
java -jar PurchaseScheduler.jar --excel=[path to excel] --repeat=[mmHHdd] --captchakey=SOMEKEY
Where --repeat=[mmHHdd] a parameter indicating a unit and its value to repeat
Examples: --repeat=050000 means each 5 min, 300100 repeat once per 1:30 hours
HINT: To see the help message just run the app without arguments like: java -jar PurchaseScheduler.jar

* All the data is kept within memory and gets parsed once the application is started
* The app polls periodically (once per 5 seconds) for external changes and syncs them with in-memory data
* The app blocks the excel file for reading/writing while purchases orders
* The purchase service takes a random address for each single purchase with equal probability
* The purchase service takes into account all the orders having the scheduled date equal or before current running time
* If an order was eligible for processing, but for some reason was not processed - it will be picked up during the next cycle
* If the order has either error-message or order is within the [Placed Order ID] column - it will not be taken during the next cycle
* The cycle is fail-safe and cannot be dropped except for program termination
* Each purchase either failed or successful will be stored into the excel file. The error-cause or orderID will be placed into [Placed Order ID]

----------------------------------------------------------------

[Latest relase:  v1.0.0] (release/PurchaseScheduler.jar)

[Example excel file] (release/orders.xlsx)