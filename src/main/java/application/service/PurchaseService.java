package application.service;

import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.stream.Stream.of;

import java.io.File;
import java.io.IOException;
import java.net.Proxy.Type;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import app.context.services.impl.LocalContext;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.LoggerService;
import app.context.services.interfaces.SchedulerService;
import app.crawler.api.ProxyHolder;
import app.util.excel.ExcelData;
import app.util.excel.FileCopyLock;
import application.model.ScheduledOrder;
import module.model.AmazonOffer;
import module.model.AmazonOrder;
import module.model.PurchaseAccount;
import module.model.ShipingAddress;
import module.purchase.AmazonCartProcessor;
import module.purchase.PurcahseManager;

public class PurchaseService {
	private static final DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	private static final int POLL_UPDATE_INTERVAL = 5;

	private final SecureRandom random = new SecureRandom();	
	
	private final Map<String, Map<String, String>> savedSessions;
	private final SchedulerService scheduler;
	private final LoggerService log;

	private final long repeatInterval;
 	private final File excelFile;

 	private ExcelData excel;
	private long excelLastModifiedTime;

	private List<ShipingAddress> shippingAddresses;
	private List<ScheduledOrder> scheduledOrders;
	private String anticaptchaKey;
	
	public PurchaseService(File excelFile, String cron, String anticaptchaKey) {
		Context context = LocalContext.getInstance("Purchase Scheduler", true, 1);

		this.scheduler = context.getScheduler();
		this.log = context.getLogger().tag(getClass());
		this.repeatInterval = parseCron(cron);

		this.shippingAddresses = new ArrayList<>(); 
		this.scheduledOrders = new ArrayList<>();
		
		this.excelFile = excelFile;	
		this.anticaptchaKey = anticaptchaKey;
		this.savedSessions = new HashMap<>();
		
		context.setWorkingMode(Context.WorkingMode.SIMULATION);
 	}
	
	private long parseCron(String cron) {
		List<Integer> times = of(cron.replaceAll("(..)(..)(..)", "$1:$2:$3").split(":"))
			.map(Integer::valueOf)
			.collect(Collectors.toList());

		return times.get(0) + HOURS.toMinutes(times.get(1)) + DAYS.toMinutes(times.get(2));
	}

	public void start() {
		try {
			this.excel = new ExcelData(excelFile, 2);				
 		} catch (IOException e) {
			if (e.getMessage().contains("Package should contain a content type part")) 
				 throw new IllegalArgumentException("Invalid excel-file format");
			
			throw new IllegalArgumentException(e); 
		}
		
		scheduler.backgroundRunReccurent(this::pollFileUpdate, POLL_UPDATE_INTERVAL, TimeUnit.SECONDS);
		scheduler.backgroundRunReccurent(this::purchaseTask, repeatInterval, TimeUnit.MINUTES);
	}
	
	private ShipingAddress getRandomAddress() {
		return shippingAddresses.get(random.nextInt(shippingAddresses.size()));
	}
	
	private void purchaseTask() throws InterruptedException {
		log.info("Purchase cycle started");

		try (
			FileCopyLock lock = excel.createFileLock();
		){		        
			lock.aquire();
			List<ScheduledOrder> ordersToBuy = scheduledOrders.stream()
				.filter(ScheduledOrder::allowedToBuy)
				.peek(S -> S.setAddress(getRandomAddress()))
				.collect(Collectors.toList());
			
			for (ScheduledOrder order: ordersToBuy) {
				AmazonOrder amazonOrder = purchaseOrder(order);
				
				String result = amazonOrder.isSuccess() == 1 ? amazonOrder.getOrderId() : amazonOrder.getErrorMessage();
				
				order.setResultOrderId(result);				
				excel.setString(order.getSourceRow(), 9, result);
			}				 
			
			excel.writeChanges();					
			excelLastModifiedTime = excelFile.lastModified();
			
			lock.release();
			log.info("Purchase cycle finished");
		} catch (IOException e) {			
			log.info(e);
		}
	}
	
	private void pollFileUpdate() {
		if (excelLastModifiedTime != excelFile.lastModified()) {
			log.info("Excel file update detected! Reloading data...");
			
			try {
				processExcelData();
				excelLastModifiedTime = excelFile.lastModified();
			} catch (IOException e) {
				log.info("Excel read file error: "+ e.getMessage());
			}
		}
	}

	private AmazonOrder purchaseOrder(ScheduledOrder order) {		
		scheduler.purgeTasks();
		
		try {		
			PurchaseAccount account = order.getBuyerAccount();
			Map<String, String> session = savedSessions.get(account.user().getValue());
			
			if (session == null) {
				account.login(anticaptchaKey);
				savedSessions.put(account.user().getValue(), account.getLoginSession());
			} else {
				account.assignActiveSession(session);
			}

			PurcahseManager buyer = new PurcahseManager(account, scheduler);

			if (account.proxyIp().get() != null) {
				String ip = account.proxyIp().get();
				Integer port = account.proxyPort().get();
				String user = account.proxyUser().get();
				String password = account.proxyPassword().get();

				buyer.buyThroughProxy(ProxyHolder.valueOf(ip, port, user, password, Type.HTTP));
			}

			AmazonCartProcessor cart = buyer.getCart();
			AmazonOffer offer = buyer.getBestOffer(order.getAsin(), null, order.isPrime(), false);

			offer.setExpectedPrice(1E6);
			offer.setExpectedQuantity(order.getQuantity());

			cart.clear();
			cart.shipTo(order.getShippingAddress());
			cart.add(buyer.rebuyOffer(offer), offer.expectedQuantity(), offer.expectedPrice());

			return cart.checkout().get(0);
		} catch (Exception e) {
			return AmazonOrder.failed(e.getMessage());
		}
	}

	private void processExcelData() throws IOException {	
		shippingAddresses.clear();
		scheduledOrders.clear();		
		excel.reload();
		
		excel.getData(1).forEachRemaining(R -> {
			shippingAddresses.add(new ShipingAddress()
				.setName(excel.checkAndGetCell(R, 0, "Recipient Name"))
				.setAddressLine1(excel.checkAndGetCell(R, 1, "Ship address 1"))
				.setCity(excel.checkAndGetCell(R, 2, "City"))
				.setState(excel.checkAndGetCell(R, 3, "State"))
				.setZip(excel.checkAndGetCell(R, 4, "Zip code"))
				.setCountry(excel.checkAndGetCell(R, 5, "Country"))
				.setPhone(excel.checkAndGetCell(R, 6, "Phone number"))				
			);
		});
 
		excel.getData(2).forEachRemaining(R -> {
			scheduledOrders.add(new ScheduledOrder()
				.setAsin(excel.checkAndGetCell(R, 0, "Asin"))
				.setUser(excel.checkAndGetCell(R, 1, "Buyer Username"))
				.setPassword(excel.checkAndGetCell(R, 2, "Buyer Password"))
				.setPrime("yes".equalsIgnoreCase(excel.checkAndGetCell(R, 3, "is Prime", true)))
				.setProxyIp(excel.checkAndGetCell(R, 4, "Proxy Ip", true))
				.setProxyPort(excel.checkAndGetIntCell(R, 5, "Proxy Port"))					
				.setProxyUser(excel.checkAndGetCell(R, 6, "Proxy User"))
				.setProxyPassword(excel.checkAndGetCell(R, 7, "Proxy Password"))
				.setScheduledOn(LocalDateTime.parse(excel.checkAndGetCell(R, 8, "Scheduled On"), FORMAT))
				.setResultOrderId(excel.checkAndGetCell(R, 9, "Order id", true))
				.setSourceRow(R)
			);
		});
		
		log.debug("Data reloaded");
	}	
}