package application.model;

import java.time.LocalDateTime;

import org.apache.poi.ss.usermodel.Row;

import module.model.PurchaseAccount;
import module.model.ShipingAddress;

public class ScheduledOrder {
	private String asin;
	private String resultOrderId;
	private LocalDateTime scheduledOn;
	private boolean isPrime;
	private int quantity;

	private PurchaseAccount buyerAccount;
	private ShipingAddress shippingAddress;
	private Row sourceRow;

	public ScheduledOrder() {
		quantity = 1;
		buyerAccount = new PurchaseAccount();
	}

	public PurchaseAccount getBuyerAccount() {
		return buyerAccount;
	}

	public String getAsin() {
		return asin;
	}

	public boolean allowedToBuy() {
		return resultOrderId == null && scheduledOn != null && scheduledOn.isBefore(LocalDateTime.now());
	}

	public boolean isPrime() {
		return isPrime;
	}

	public LocalDateTime getScheduledOn() {
		return scheduledOn;
	}

	public String getResultOrderId() {
		return resultOrderId;
	}
	
	public ShipingAddress getShippingAddress() {
		return shippingAddress;
	}

	public ScheduledOrder setAsin(String asin) {
		this.asin = asin;
		return this;
	}
	
	public ScheduledOrder setSourceRow(Row sourceRow) {
		this.sourceRow = sourceRow;
		return this;
	}
	
	public ScheduledOrder setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	public ScheduledOrder setUser(String user) {
		this.buyerAccount.user().set(user);
		return this;
	}

	public ScheduledOrder setPassword(String password) {
		this.buyerAccount.password().set(password);
		return this;
	}

	public ScheduledOrder setProxyIp(String proxyIp) {
		this.buyerAccount.proxyIp().set(proxyIp);
		return this;
	}

	public ScheduledOrder setProxyPort(Integer port) {
		this.buyerAccount.proxyPort().set(port);
		return this;
	}

	public ScheduledOrder setProxyUser(String proxyUser) {
		this.buyerAccount.proxyUser().set(proxyUser);
		return this;
	}

	public ScheduledOrder setProxyPassword(String proxyPassword) {
		this.buyerAccount.proxyPassword().set(proxyPassword);
		return this;
	}

	public ScheduledOrder setPrime(boolean isPrime) {
		this.isPrime = isPrime;
		return this;
	}

	public ScheduledOrder setScheduledOn(LocalDateTime scheduledOn) {
		this.scheduledOn = scheduledOn;
		return this;
	}

	public ScheduledOrder setResultOrderId(String resultOrderId) {
		this.resultOrderId = resultOrderId;
		return this;
	}

	public ScheduledOrder setAddress(ShipingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
		return this;
	}

	public int getQuantity() {
		return quantity;
	}

	public Row getSourceRow() {
 		return sourceRow;
	}
}
