package application;

import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import application.service.PurchaseService;

public final class PurchaseScheduler {
	private static final String USAGE_HELP_LINE_1 = "Usage: java -jar PurchaseScheduler.jar --excel=[path to excel] --repeat=[mmHHdd] --captchakey=SOMEKEY";
	private static final String USAGE_HELP_LINE_2 = "Where --repeat=[mmHHdd] a parameter indicating a unit and its value to repeat";
	private static final String USAGE_HELP_LINE_3 = "\nExamples: --repeat=050000 means each 5 min, 300100 repeat once per 1:30 hours";

	private PurchaseScheduler() {
	}
	
	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println(USAGE_HELP_LINE_1);
			System.out.println(USAGE_HELP_LINE_2);
			System.out.println(USAGE_HELP_LINE_3);
			
			return;
		}		

		Map<String, String> params = parseParams(args);		
		launch(params);
	}

	private static void launch(Map<String, String> params) {		
		File excel = new File(params.getOrDefault("excel", "."));
		String cron = params.getOrDefault("repeat", "");
		String anticaptchaKey = params.get("captchakey");
		
		if (!excel.exists() || excel.isDirectory()) {
			System.out.println("Cannot access the specified excel file");
			return;
		}
		
		if (!excel.getName().endsWith("xlsx")) {
			System.out.println("only XLSX files are supported");
			return;
		}
		
		if (!cron.matches("\\d{6}")) {
			System.out.println("Repeat paramanter is not valid");
			return;
		}
		
		if (anticaptchaKey == null) {
			System.out.println("Please set anticaptcha key as --captchaKey=SOMEKEY");
			return;
		}
		
		PurchaseService purchaseSevrice = new PurchaseService(excel, cron, anticaptchaKey); 
		purchaseSevrice.start();
	}
	
	private static Map<String, String> parseParams(String[] args) {
		return Stream.of(args).flatMap(A -> Arrays.stream(A.split(" ")))
			.filter(P -> P.matches("^--.*?=.*$"))
			.map(P -> P.split("(--)|="))
			.collect(
				Collectors.toMap(P -> P[1].toLowerCase(), P -> P[2])
			);
	}
}
